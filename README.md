# uBlock Origin - My rules

Since Firefox/[uBO][repo] likes to delete my rules ever so often, I'm
keeping a copy of them in this repo for safekeeping.

To use these rules or get more clarification, follow the steps on [uBO's
documentation][doc].

[repo]: https://github.com/gorhill/uBlock
[doc]: https://github.com/gorhill/uBlock/wiki
